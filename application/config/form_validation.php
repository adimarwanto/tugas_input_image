<?php
$config = array(
    'validation/configuration' => array(
        array(
            'field' => 'max_text_field',
            'label' => 'Text Field Three',
            'rules' => 'required|max_length[20]'
        )
    )
);