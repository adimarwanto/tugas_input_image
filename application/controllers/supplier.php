<?php
defined('BASEPATH') OR Exit('No direct script access allowed');

class supplier extends CI_Controller
{
	Public function __construct()
	{
		parent::__construct();
		//load model terkait
		$this->load->model("supplier_model");

		$user_login = $this->session->userdata();
		if (count($user_login)<=1) {
			# code...
			redirect("auth/index", "refresh");
		}
	}	
	public function index()
	{
		$this->listsupplier();
	}
	 public function listsupplier()
	{
		 		$data['data_supplier'] = $this->supplier_model->tampilDataSupplier();
		 		$data['content']	   = 'forms/list_supplier';
		$this->load->view('home1', $data); 
	}
	public function inputSupplier()
	{
		$data['data_supplier'] = $this->supplier_model->tampilDataSupplier();
		$data['content']	   = 'forms/input_supplier';
		//if(!empty($_REQUEST)){
			//$m_supplier = $this->supplier_model;
			//$m_supplier->save();
			//redirect("supplier/index", "refresh");
			//}
		$validation = $this->form_validation;
		$validation->set_rules($this->supplier_model->rules());

		if ($validation->run()) {
			# code...
			$this->supplier_model->save();
			$this->session->set_flashdata('info', '<div style="color: green">Simpan data Berhasil
				!</div>');
			redirect("supplier/index", "refresh");
		}
		$this->load->view('home1',$data); 	
	}
	public function detailsupplier($kode_supplier)
	{
				$data['detail_supplier'] = $this->supplier_model->detail($kode_supplier);
				$data['content']	   = 'forms/detail_supplier';
		$this->load->view('home1', $data); 	
	}
	public function edit_s($kode_supplier)
	{
		$data['detail_supplier'] = $this->supplier_model->edit_detail($kode_supplier);
		$data['content']	   = 'forms/edit_supplier';
		//if(!empty($_REQUEST)){
			//$m_supplier = $this->supplier_model;
			//$m_supplier->update($kode_supplier);
			//redirect("supplier/index", "refresh");
			//}

		$validation = $this->form_validation;
		$validation->set_rules($this->supplier_model->rules());

		if ($validation->run()) {
			# code...
			$this->supplier_model->update($kode_supplier);
			$this->session->set_flashdata('info', '<div style="color: green">Simpan data Berhasil
				!</div>');
			redirect("supplier/index", "refresh");
		}
		$this->load->view('home1',$data); 	
	}
}