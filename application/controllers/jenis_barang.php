<?php
defined('BASEPATH') OR Exit('No direct script access allowed');

class jenis_barang extends CI_Controller
{
	Public function __construct()
	{
		parent::__construct();
		//load model terkait
		$this->load->model("jenis_model");

		$user_login = $this->session->userdata();
		if (count($user_login)<=1) {
			# code...
			redirect("auth/index", "refresh");
		}
	}	
	public function index()
	{
		$this->listjenis();
	}
	 public function listjenis()
	{
	$data['data_jenis'] = $this->jenis_model->tampilDataJenis();
	$data['content']	   = 'forms/list_jenis';
		$this->load->view('home1', $data); 
	}
	public function inputjenis()
	{
		$data['data_jenis'] = $this->jenis_model->tampilDataJenis();
		$data['content']	   = 'forms/input_jenis';
		//if(!empty($_REQUEST)){
			//$m_jenis = $this->jenis_model;
			//$m_jenis->save();
			//redirect("jenis_barang/index", "refresh");
			//}

		$validation = $this->form_validation;
		$validation->set_rules($this->jenis_model->rules());

		if ($validation->run()) {
			# code...
			$this->jenis_model->save();
			$this->session->set_flashdata('info', '<div style="color: green">Simpan data Berhasil
				!</div>');
			redirect("jenis_barang/index", "refresh");
		}
		$this->load->view('home1',$data); 	
	}
	public function detail_jenis($kode_jenis)
	{
				$data['detail_jenis'] = $this->jenis_model->detail($kode_jenis);
				$data['content']	   = 'forms/detail_jenis';
		$this->load->view('home1', $data); 	
	}
	public function edit_j($kode_jenis)
	{
		$data['detail_jenis'] = $this->jenis_model->edit_detail($kode_jenis);
		$data['content']	   = 'forms/edit_jenis';
		//if(!empty($_REQUEST)){
			//$m_jenis = $this->jenis_model;
			//$m_jenis->update($kode_jenis);
			//redirect("jenis_barang/index", "refresh");
			//}
		$validation = $this->form_validation;
		$validation->set_rules($this->jenis_model->rules());

		if ($validation->run()) {
			# code...
			$this->jenis_model->update($kode_jenis);
			$this->session->set_flashdata('info', '<div style="color: green">Simpan data Berhasil
				!</div>');
			redirect("jenis_barang/index", "refresh");
		}
		$this->load->view('home1',$data); 	
	}
}