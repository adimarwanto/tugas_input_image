<?php
defined('BASEPATH') OR Exit('No direct script access allowed');

class jabatan extends CI_Controller
{
	Public function __construct()
	{
		parent::__construct();
		//load model terkait
		$this->load->model("jabatan_model");

		$user_login = $this->session->userdata();
		if (count($user_login)<=1) {
			# code...
			redirect("auth/index", "refresh");
		}
	}	
	public function index()
	{
		$this->listjabatan();
	}
	 public function listjabatan()
	{
		$data['data_jabatan'] = $this->jabatan_model->tampilDataJabatan();
		$data['content']	   = 'forms/list_jabatan';
		$this->load->view('home1', $data); 
	}
	public function inputJabatan()
	{
				$data['data_jabatan'] = $this->jabatan_model->tampilDataJabatan();
				$data['content']	   = 'forms/input_jabatan';

		//if(!empty($_REQUEST)){
			//$m_jabatan = $this->jabatan_model;
			//$m_jabatan->save();
			//redirect("jabatan/index", "refresh");
			//}

		$validation = $this->form_validation;
		$validation->set_rules($this->jabatan_model->rules());

		if ($validation->run()) {
			# code...
			$this->jabatan_model->save();
			$this->session->set_flashdata('info', '<div style="color: green">Simpan data Berhasil
				!</div>');
			redirect("jabatan/index", "refresh");
		}
		$this->load->view('home1',$data);
	}
	public function detailjabatan($kode_jabatan)
	{
			$data['detail_jabatan'] = $this->jabatan_model->detail($kode_jabatan);
			$data['content']	   = 'forms/detail_jabatan';
		$this->load->view('home1', $data); 	
	}	
		public function edit_j($kode_jabatan)
	{
		
		$data['detail_jabatan'] = $this->jabatan_model->edit_jabatan($kode_jabatan);
		$data['content']	   = 'forms/edit_jabatan';
		//if(!empty($_REQUEST)){
			//$m_jabatan = $this->jabatan_model;
			//$m_jabatan->update($kode_jabatan);
			//redirect("jabatan/index", "refresh");
			//}

		$validation = $this->form_validation;
		$validation->set_rules($this->jabatan_model->rules());

		if ($validation->run()) {
			# code...
			$this->jabatan_model->update($kode_jabatan);
			$this->session->set_flashdata('info', '<div style="color: green">Simpan data Berhasil
				!</div>');
			redirect("jabatan/index", "refresh");
		}
		$this->load->view('home1',$data);
	}



}