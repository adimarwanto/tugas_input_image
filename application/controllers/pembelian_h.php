<?php
defined('BASEPATH') OR Exit('No direct script access allowed');

class pembelian_h extends CI_Controller
{
	Public function __construct()
	{
		parent::__construct();
		//load model terkait (untuk manggil scrip pertama kali di jalankan )
		$this->load->model("pembelian_h_model");
		$this->load->model("supplier_model");
		$this->load->model("barang_model");

		$user_login = $this->session->userdata();
		if (count($user_login)<=1) {
			# code...
			redirect("auth/index", "refresh");
		}
	}	
	public function index()
	{
		$this->listpembelian_h();
	}
	 public function listpembelian_h()
	{
		$data['data_pembelian'] = $this->pembelian_h_model->tampilDatapembelian_h2();
		
		$this->load->view('input_pembelian', $data); 
	}

	public function inputPembelian()
	{
		$data['data_supplier'] = $this->supplier_model->tampilDataSupplier();
		$data['content']	   = 'forms/input_pembelian';
		//if(!empty($_REQUEST)){
			//$m_pembelian_h = $this->pembelian_h_model;
			//$m_pembelian_h->save();
			//panggil trans terakhir
			//$id_terakhir = $m_pembelian_h->idTransaksiTerakhir();
			//redirect("pembelian_h/inputDetail/". $id_terakhir, "refresh");
			//}

		$validation = $this->form_validation;
		$validation->set_rules($this->pembelian_h_model->rules());

		if ($validation->run()) {
			# code...
			$m_pembelian_h = $this->pembelian_h_model;
			$this->pembelian_h_model->save();
			$this->session->set_flashdata('info', '<div style="color: green">Simpan data Berhasil
				!</div>');
			$id_terakhir = $m_pembelian_h->idTransaksiTerakhir();
			redirect("pembelian_h/inputDetail/". $id_terakhir, "refresh");
		}
		$this->load->view('home1',$data); 	
	}
	
	 public function inputDetail($id_pembelian_header)
	{
        // panggil data barang untuk kebutuhan form input
        $data['data_barang'] 	= $this->barang_model->tampilDataBarang();
        $data['id_header'] 	    = $id_pembelian_header;
        $data['data_pembelian_detail'] 	= $this->pembelian_h_model->tampilDataPembelianDetail($id_pembelian_header);
        $data['content']	   = 'forms/input_pembelian_d';
        // proses simpan ke pembelian detail jika ada request form
       // if (!empty($_REQUEST)) {
            //save detail
            //$this->pembelian_h_model->savePembelianDetail($id_pembelian_header);
            
            //proses update stok
            //$kode_barang  = $this->input->post('kode_barang');
            //$qty        = $this->input->post('qty');
            //$this->barang_model->updateStok($kode_barang, $qty);

			//redirect("pembelian_h/inputDetail/" . $id_pembelian_header, "refresh");
			//redirect("pembelian/inputdetail/" . $id_pembelian_header, "refresh");
        //}

        $validation = $this->form_validation;
		$validation->set_rules($this->pembelian_h_model->rules1());

		if ($validation->run()) {
			# code...
			$m_pembelian_h = $this->pembelian_h_model;
			$this->pembelian_h_model->savePembelianDetail($id_pembelian_header);
			$this->session->set_flashdata('info', '<div style="color: green">Simpan data Berhasil
				!</div>');
			$kode_barang  = $this->input->post('kode_barang');
            $qty        = $this->input->post('qty');
            $this->barang_model->updateStok($kode_barang, $qty);
			redirect("pembelian_h/inputDetail/" . $id_pembelian_header, "refresh");
		}
        
		//$data['content'] 		= 'forms/input_pembelian_detail';
		$this->load->view('home1', $data);
	}

	//public function inputDetail($id_pembelian_header)
	//{
		//panggil data barang untuk kebutuhan form input
		//$data['data_barang'] = $this->barang_model->tampilDataBarang();
		//$data['id_header'] = $id_pembelian_header;
		//$data['data_pembelian_detail'] = $this->pembelian_h_model->tampilDataPembelianDetail($id_pembelian_header);

		//if (!empty($_REQUEST)) 
		//{
				# code...
			// save detail
			//$this->pembelian_h_model->savePemebelianDetail($id_pembelian_header);

			//proses update stok
			//$kd_barang =$this->input->post('kode_barang');
			//$qty =$this->input->post('qty');
			//$this->barang_model->updateStock($kd_barang, $qty);
			//redirect("pemebelian_h/inputDetail/" . $id_pembelian_header." refresh");
		//}
		//$this->load->view('input_pembelian_d',$data); 

	//}
	//public function listdetailkaryawan($nik)
	//{
				//$data['detail_karyawan'] = $this->karyawan_model->detail($nik);
		//$this->load->view('detail_k', $data); 	
	//}
	//public function edit($nik)
	//{
				//$data['data_jabatan'] = $this->jabatan_model->tampilDataJabatan();
				//$data['detail_karyawan'] = $this->karyawan_model->detail($nik);
			//if(!empty($_REQUEST)){
			//$m_karyawan = $this->karyawan_model;
			//$m_karyawan->update($nik);
			//redirect("karyawan/index", "refresh");
			//}
		//$this->load->view('edit_karyawan', $data); 	
	//}
}