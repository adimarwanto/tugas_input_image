<?php defined('BASEPATH') OR exit('No direct script acces allowed');

class jenis_model extends CI_Model
{
	//panggil nama table
	private $_table = "jenis_barang";

	public function rules()
	{
		return
		[
			[
				'field' => 'kode_jenis',
				'label'	=> 'kode jenis',
				'rules' => 'required|max_length[5]',
				'errors' =>[
					'required' => 'kode jenis tidak Boleh Kosong',
					'max_length' => 'kode jenis tdak Boleh Lebih dari 5 karakter',
				],
			],
			[
				'field' => 'nama_jenis',
				'label'	=> 'nama jenis',
				'rules' => 'required',
				'errors' =>[
					'required' => 'nama jenis tidak Boleh Kosong',
				],
			]
		];
	}
	
	public function tampilDataJenis()
	{
		// seperti : select * from <nama_table>
		return $this->db->get($this->_table)->result();
	}

	public function tampilDataJenis2()
	{
		//menggunakan query
		$query = $this->db->query("SELECT * FROM jenis_barang where flag = 1");
		return $query->result();
	}

	public function tampilDatajenis3()
	{
		//menggunakan query 
		$this->db->select('*');
		$this->db->order_by('kode_jenis', 'ASC');
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	public function save()
	{
		$tgl = $this->input->post('tgl');
		$bln = $this->input->post('bln');
		$thn = $this->input->post('thn');
		$tgl_gabungan = $thn."-".$bln."-".$tgl;
		
		$data['kode_jenis']			= $this->input->post('kode_jenis');
		$data['nama_jenis']			= $this->input->post('nama_jenis');
		$data['flag']				= 1;
		$this->db->insert($this->_table, $data);
	}

	public function update($kode_jenis)
	{	
		$data['kode_jenis']			= $this->input->post('kode_jenis');
		$data['nama_jenis']			= $this->input->post('nama_jenis');
		$data['flag']				= 1;
		$this->db->where('kode_jenis', $kode_jenis);
		$this->db->update($this->_table, $data);
	}
	public function detail($kode_jenis)
	{
		$this->db->select('*');
		$this->db->where('kode_jenis', $kode_jenis);
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();	
	}
		public function edit_detail($kode_jenis)
	{
		$this->db->select('*');
		$this->db->where('kode_jenis', $kode_jenis);
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();	
	}
	
	
	
}