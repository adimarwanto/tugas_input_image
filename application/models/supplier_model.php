<?php defined('BASEPATH') OR exit('No direct script acces allowed');

class supplier_model extends CI_Model
{
	//panggil nama table
	private $_table = "supplier";

	public function rules()
	{
		return
		[
			[
				'field' => 'kode_supplier',
				'label'	=> 'kode supplier',
				'rules' => 'required|max_length[5]',
				'errors' =>[
					'required' => 'kode supplier tidak Boleh Kosong',
					'max_length' => 'kode supplier tdak Boleh Lebih dari 5 karakter',
				],
			],
			[
				'field' => 'nama_supplier',
				'label'	=> 'nama supplier',
				'rules' => 'required',
				'errors' =>[
					'required' => 'nama supplier tidak Boleh Kosong',
				],
			],
			[
				'field' => 'alamat',
				'label'	=> 'alamat',
				'rules' => 'required',
				'errors' =>[
					'required' => 'alamat tidak Boleh Kosong',
				],
			],
			[
				'field' => 'telp',
				'label'	=> 'telp',
				'rules' => 'required|max_length[15]',
				'errors' =>[
					'required' => 'telp tidak Boleh Kosong',
					'max_length' => 'telp tdak Boleh Lebih dari 15 karakter',	
				],
			],
		];
	}
	
	public function tampilDataSupplier()
	{
		// seperti : select * from <nama_table>
		return $this->db->get($this->_table)->result();
	}

	public function tampilDataSupplier2()
	{
		//menggunakan query
		$query = $this->db->query("SELECT * FROM supplier where flag = 1");
		return $query->result();
	}

	public function tampilDataSupplier3()
	{
		//menggunakan query 
		$this->db->select('*');
		$this->db->order_by('kode_supplier', 'ASC');
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	public function save()
	{
		$tgl = $this->input->post('tgl');
		$bln = $this->input->post('bln');
		$thn = $this->input->post('thn');
		$tgl_gabungan = $thn."-".$bln."-".$tgl;
		
		$data['kode_supplier']			= $this->input->post('kode_supplier');
		$data['nama_supplier']			= $this->input->post('nama_supplier');
		$data['alamat']					= $this->input->post('alamat');
		$data['telp']					= $this->input->post('telp');
		$data['flag']					= 1;
		$this->db->insert($this->_table, $data);
	}
	public function update($kode_supplier)
	{		
		$data['kode_supplier']			= $this->input->post('kode_supplier');
		$data['nama_supplier']			= $this->input->post('nama_supplier');
		$data['alamat']					= $this->input->post('alamat');
		$data['telp']					= $this->input->post('telp');
		$data['flag']					= 1;
		$this->db->where('kode_supplier', $kode_supplier);
		$this->db->update($this->_table, $data);
	}
	public function detail($kode_supplier)
	{
		$this->db->select('*');
		$this->db->where('kode_supplier', $kode_supplier);
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();	
	}
	public function edit_detail($kode_supplier)
	{
		$this->db->select('*');
		$this->db->where('kode_supplier', $kode_supplier);
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();	
	}
	
	
}