-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 01, 2019 at 02:21 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `toko_jaya_abadi`
--
CREATE DATABASE IF NOT EXISTS `toko_jaya_abadi` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `toko_jaya_abadi`;

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `kode_barang` varchar(5) NOT NULL,
  `nama_barang` varchar(150) NOT NULL,
  `harga_barang` float NOT NULL,
  `kode_jenis` varchar(5) NOT NULL,
  `flag` int(11) NOT NULL,
  `stock` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`kode_barang`, `nama_barang`, `harga_barang`, `kode_jenis`, `flag`, `stock`) VALUES
('', '', 0, 'JN001', 1, 0),
('BR001', 'HP', 4500000, 'JN001', 1, 32),
('BR002', 'Handphone', 1200000, 'JN002', 1, 4),
('BR003', 'Sempak', 1200000, 'JN003', 1, 4),
('BR004', 'mobil', 1200000, 'JN003', 1, 5),
('BR005', 'mobil', 12312300, 'JN003', 1, 5);

-- --------------------------------------------------------

--
-- Table structure for table `jabatan`
--

CREATE TABLE `jabatan` (
  `kode_jabatan` varchar(5) NOT NULL,
  `nama_jabatan` varchar(100) NOT NULL,
  `keterangan` text NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jabatan`
--

INSERT INTO `jabatan` (`kode_jabatan`, `nama_jabatan`, `keterangan`, `flag`) VALUES
('JB001', 'Manager', 'Operasional', 1),
('JB002', 'Admin', 'Operasional', 1),
('JB003', 'OB', 'Opersional', 1),
('JB004', 'Staff', 'Operasional', 1);

-- --------------------------------------------------------

--
-- Table structure for table `jenis_barang`
--

CREATE TABLE `jenis_barang` (
  `kode_jenis` varchar(5) NOT NULL,
  `nama_jenis` varchar(100) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_barang`
--

INSERT INTO `jenis_barang` (`kode_jenis`, `nama_jenis`, `flag`) VALUES
('JN001', 'Alat Tulis Kantor', 1),
('JN002', 'Pulpen', 1),
('JN003', 'Transportasi', 1);

-- --------------------------------------------------------

--
-- Table structure for table `karyawan`
--

CREATE TABLE `karyawan` (
  `nik` varchar(10) NOT NULL,
  `nama_lengkap` varchar(150) NOT NULL,
  `tempat_lahir` varchar(100) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `jenis_kelamin` varchar(1) NOT NULL,
  `alamat` text NOT NULL,
  `telp` varchar(15) NOT NULL,
  `kode_jabatan` varchar(5) NOT NULL,
  `flag` int(11) NOT NULL,
  `photo` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `karyawan`
--

INSERT INTO `karyawan` (`nik`, `nama_lengkap`, `tempat_lahir`, `tgl_lahir`, `jenis_kelamin`, `alamat`, `telp`, `kode_jabatan`, `flag`, `photo`) VALUES
('1902001', 'Cecep Maricep', 'Jakarta', '2010-02-16', 'L', 'Jl.Mawar Panjang Jakarta Utara', '082122221111', 'JB001', 1, ''),
('1902002', 'Sudirman', 'Lampung', '2019-01-13', 'L', 'Jl.ManaSajakusuka', '0821555565612', 'JB002', 1, ''),
('1902003', 'nisa', 'Kota Jakarta Utara', '1959-01-01', 'P', 'jl warakas gang 19 no 10a rt 012 rw 014        ', '082155559971', 'JB001', 1, ''),
('1902004', 'didik', 'Kota Jakarta Utara', '1959-01-01', 'L', 'jl warakas gang 19 no 10a rt 012 rw 014  ', '082155559971', 'JB001', 1, '20190401_1902004.jpg'),
('1902005', 'nisa', 'Kota Jakarta Utara', '1959-01-01', 'P', 'jl warakas gang 19 no 10a rt 012 rw 014', '082155559971', 'JB001', 1, 'default.png'),
('1902006', 'didi', 'jakarta', '2010-04-08', 'L', 'Jl.waraksa', '02188838383', 'JB001', 0, ''),
('1902007', 'Joni', 'Kota Jakarta Utara', '1959-01-01', 'L', 'jl warakas gang 19 no 10a rt 012 rw 014', '082155559971', 'JB001', 1, '20190401_1902007.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `pembelian_d`
--

CREATE TABLE `pembelian_d` (
  `id_pembelian_d` int(11) NOT NULL,
  `id_pembelian_h` int(11) NOT NULL,
  `kode_barang` varchar(5) NOT NULL,
  `qty` int(11) NOT NULL,
  `harga` float NOT NULL,
  `jumlah` float NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembelian_d`
--

INSERT INTO `pembelian_d` (`id_pembelian_d`, `id_pembelian_h`, `kode_barang`, `qty`, `harga`, `jumlah`, `flag`) VALUES
(1, 21, 'BR001', 4, 200, 800, 1),
(2, 25, 'BR001', 4, 200, 800, 1),
(3, 25, 'BR001', 4, 200, 800, 1),
(4, 25, 'BR001', 4, 200, 800, 1),
(5, 25, 'BR001', 0, 0, 0, 1),
(6, 25, 'BR001', 0, 0, 0, 1),
(7, 25, 'BR001', 0, 0, 0, 1),
(8, 25, 'BR001', 0, 0, 0, 1),
(9, 25, 'BR001', 20, 1000, 20000, 1),
(10, 25, 'BR001', 20, 1000, 20000, 1),
(11, 26, 'BR001', 4, 800, 3200, 1),
(12, 28, 'BR001', 4, 200, 800, 1),
(13, 29, 'BR003', 4, 200, 800, 1),
(14, 37, 'BR001', 4, 200, 800, 1),
(15, 38, 'BR002', 4, 200, 800, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pembelian_h`
--

CREATE TABLE `pembelian_h` (
  `id_pembelian_h` int(11) NOT NULL,
  `no_trans` varchar(10) NOT NULL,
  `kode_supplier` varchar(5) NOT NULL,
  `tanggal` date NOT NULL,
  `approved` int(11) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembelian_h`
--

INSERT INTO `pembelian_h` (`id_pembelian_h`, `no_trans`, `kode_supplier`, `tanggal`, `approved`, `flag`) VALUES
(1, 'BS-10', 'SP002', '2019-02-14', 1, 1),
(6, 'BS-101', 'SP001', '2019-02-14', 1, 1),
(7, 'BS-101', 'SP001', '2019-02-14', 1, 1),
(8, 'BS-101', 'SP002', '2019-02-19', 1, 1),
(9, 'BS-101', 'SP002', '2019-02-19', 1, 1),
(12, '', '', '2019-02-19', 1, 1),
(13, 'BS-101', '', '2019-02-19', 1, 1),
(14, 'BS-101', '', '2019-02-19', 1, 1),
(15, 'BS-101', '', '2019-02-19', 1, 1),
(16, '', '', '2019-02-19', 1, 1),
(17, '', '', '2019-02-19', 1, 1),
(18, '', '', '2019-02-19', 1, 1),
(19, '1231', 'SP001', '2019-02-19', 1, 1),
(20, '', '', '2019-02-19', 1, 1),
(21, '', '', '2019-02-19', 1, 1),
(22, '', '', '2019-02-19', 1, 1),
(23, '', '', '2019-02-19', 1, 1),
(24, '', '', '2019-02-19', 1, 1),
(25, 'BS-10', 'SP002', '2019-02-19', 1, 1),
(26, 'BS-10', 'SP001', '2019-02-22', 1, 1),
(27, '', '', '2019-03-22', 1, 1),
(28, 'BS-10', 'SP002', '2019-03-26', 1, 1),
(29, 'BS-101', 'SP001', '2019-03-26', 1, 1),
(30, 'BS-101', 'SP001', '2019-03-26', 1, 1),
(31, 'BS-101', 'SP001', '2019-03-26', 1, 1),
(32, 'BS-101', 'SP001', '2019-03-26', 1, 1),
(33, 'BS-101', 'SP001', '2019-03-26', 1, 1),
(34, 'BS-101', 'SP001', '2019-03-26', 1, 1),
(35, 'BS-101', 'SP001', '2019-03-26', 1, 1),
(36, 'BS-101', 'SP001', '2019-03-26', 1, 1),
(37, 'BS-101', 'SP001', '2019-03-26', 1, 1),
(38, 'BS-101', 'SP001', '2019-03-26', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `penjualan_d`
--

CREATE TABLE `penjualan_d` (
  `id_jual_d` int(11) NOT NULL,
  `id_jual_h` int(11) NOT NULL,
  `kode_barang` varchar(5) NOT NULL,
  `qty` int(11) NOT NULL,
  `harga` float NOT NULL,
  `jumlah` float NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `penjualan_h`
--

CREATE TABLE `penjualan_h` (
  `id_jual_h` int(11) NOT NULL,
  `no_trans` varchar(10) NOT NULL,
  `tanggal` date NOT NULL,
  `approved` int(11) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `kode_supplier` varchar(5) NOT NULL,
  `nama_supplier` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `telp` varchar(15) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`kode_supplier`, `nama_supplier`, `alamat`, `telp`, `flag`) VALUES
('', '', '', '', 1),
('SP001', 'CV.Informa Abadi', 'Jl.Mangga Dua Raya No 10', '0213383838', 1),
('SP002', 'dika', 'jl warakas gang 19 no 10a rt 012 rw 014    ', '082155559971', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `nik` varchar(10) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(32) NOT NULL,
  `tipe` int(11) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `nik`, `email`, `password`, `tipe`, `flag`) VALUES
(1, '1902001', 'user@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 2, 1),
(2, 'admin', 'admin@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`kode_barang`);

--
-- Indexes for table `jabatan`
--
ALTER TABLE `jabatan`
  ADD PRIMARY KEY (`kode_jabatan`);

--
-- Indexes for table `karyawan`
--
ALTER TABLE `karyawan`
  ADD PRIMARY KEY (`nik`);

--
-- Indexes for table `pembelian_d`
--
ALTER TABLE `pembelian_d`
  ADD PRIMARY KEY (`id_pembelian_d`);

--
-- Indexes for table `pembelian_h`
--
ALTER TABLE `pembelian_h`
  ADD PRIMARY KEY (`id_pembelian_h`);

--
-- Indexes for table `penjualan_d`
--
ALTER TABLE `penjualan_d`
  ADD PRIMARY KEY (`id_jual_d`);

--
-- Indexes for table `penjualan_h`
--
ALTER TABLE `penjualan_h`
  ADD PRIMARY KEY (`id_jual_h`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`kode_supplier`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pembelian_d`
--
ALTER TABLE `pembelian_d`
  MODIFY `id_pembelian_d` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `pembelian_h`
--
ALTER TABLE `pembelian_h`
  MODIFY `id_pembelian_h` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `penjualan_d`
--
ALTER TABLE `penjualan_d`
  MODIFY `id_jual_d` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `penjualan_h`
--
ALTER TABLE `penjualan_h`
  MODIFY `id_jual_h` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
